from django.shortcuts import render
from .models import TodoList, TodoItem
# Create your views here.


def get_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "get_list": todo_list
    }
    return render(request, "todos/list.html", context)


def get_list_detail(request, id):
    todo_detail = TodoList.objects.get(id=id)
    context = {
        "get_list_detail": todo_detail
    }
    return render(request, "todos/list-detail.html", context)
