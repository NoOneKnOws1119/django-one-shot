from django.urls import path
from todos.views import get_list, get_list_detail
from django.shortcuts import redirect


def redirect_to_todo(request):
    return redirect("todo_list")


urlpatterns = [
    path(("<int:id>/"), get_list_detail, name="get_list_detail"),
    path("", get_list, name="get_list"),
]
